<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Account - Bootstrap Admin Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="<?= base_url() ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url() ?>css/style.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>
<body>

  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">
        <a class="brand" href="index.html">
          MedRec.App v0.1
        </a>
      </div> <!-- /container -->
    </div> <!-- /navbar-inner -->
  </div> <!-- /navbar -->
      
  <div class="subnavbar"></div>

  <div class="main">
    <div class="main-inner">
      <div class="container">
  
        <?php if (isset ($page)) require_once(APPPATH . 'views/pages/' . $page . '.php') ?>
  
      </div> <!-- /container -->
    </div> <!-- /main-inner -->
  </div> <!-- /main -->
   
  <div class="extra"></div> <!-- /extra -->
      
  <div class="footer">
    <div class="footer-inner">
      <div class="container">
        <div class="row">
            <div class="span12">
              &copy; 2013 <a href="http://www.egrappler.com/">Bootstrap Responsive Admin Template</a>.
            </div> <!-- /span12 -->
          </div> <!-- /row -->
      </div> <!-- /container -->
    </div> <!-- /footer-inner -->
  </div> <!-- /footer -->

  <script src="<?= base_url() ?>js/jquery-1.7.2.min.js"></script>
  <script src="<?= base_url() ?>js/bootstrap.js"></script>
  <script src="<?= base_url() ?>js/base.js"></script>
  </body>

</html>