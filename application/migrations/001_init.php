<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Migration_init extends CI_Migration {

  public function up () {
    $this->db->query("DROP TABLE IF EXISTS `patient`");
    $this->db->query("
      CREATE TABLE `patient` (
        `uuid` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `address` varchar(255) NOT NULL,
        `gender` tinyint(1) NOT NULL,
        `dob` date NOT NULL,
        `contact` varchar(255) NOT NULL,
        `bloodgroups` char(2) NOT NULL,
        `allergies` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    $this->db->query("DROP TABLE IF EXISTS `visit`");
    $this->db->query("
      CREATE TABLE `visit` (
        `uuid` varchar(255) NOT NULL,
        `patient` varchar(255) NOT NULL,
        `location` varchar(255) NOT NULL,
        `visittime` datetime NOT NULL,
        `age` int(11) NOT NULL,
        `weight` int(11) NOT NULL,
        `height` int(11) NOT NULL,
        `problems` varchar(255) NOT NULL,
        `diagnose` varchar(255) NOT NULL,
        `medications` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
  }

  public function down () {
    $this->db->query("DROP TABLE IF EXISTS `patient`");
    $this->db->query("DROP TABLE IF EXISTS `visit`");
  }

}